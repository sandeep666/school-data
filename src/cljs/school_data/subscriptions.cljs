(ns school-data.subscriptions
  (:require [re-frame.core :refer [reg-sub]]))

(reg-sub
  :page
  (fn [db _]
    (:page db)))

(reg-sub
  :docs
  (fn [db _]
    (:docs db)))

(reg-sub
 :school-list
 (fn [db _]
   (:schools db)))

(reg-sub
 :current-school
 (fn [db _]
   (:schools db)))
