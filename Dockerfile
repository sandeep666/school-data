FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/school-data.jar /school-data/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/school-data/app.jar"]
