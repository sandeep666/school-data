(ns school-data.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [school-data.core-test]))

(doo-tests 'school-data.core-test)

