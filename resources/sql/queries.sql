-- :name save-school! :! :n
-- :doc "save school name"
insert into schools
(schoolname)
values (:schoolname );
