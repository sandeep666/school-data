(ns school-data.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [school-data.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[school-data started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[school-data has shut down successfully]=-"))
   :middleware wrap-dev})
