(ns user
  (:require [mount.core :as mount]
            [school-data.figwheel :refer [start-fw stop-fw cljs]]
            school-data.core))

(defn start []
  (mount/start-without #'school-data.core/http-server
                       #'school-data.core/repl-server))

(defn stop []
  (mount/stop-except #'school-data.core/http-server
                     #'school-data.core/repl-server))

(defn restart []
  (stop)
  (start))


