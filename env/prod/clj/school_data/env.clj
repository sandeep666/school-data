(ns school-data.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[school-data started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[school-data has shut down successfully]=-"))
   :middleware identity})
